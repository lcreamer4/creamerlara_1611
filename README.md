# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for?

* This repository is for my month long project for my scavenger hunt app.
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* This app is set up so that you can access the pieces of the app without having to go to any of the disney parks to test the app. For this reason, some of the pieces of the app, such as the location data for beginning the app, as well as the location data for checking to see if you are in the right area have been modified. All of the code is ready for when a user goes to one of the parks, however, at the moment, the locations for answers are hardcoded. This will be changed once the app has been graded, and before it is sent to the Google Play store.
* Configuration
* You must allow this app to access your location in order to play the game.
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Lacfencer@hotmail.com or lcreamer@fullsail.edu
* Other community or team contact