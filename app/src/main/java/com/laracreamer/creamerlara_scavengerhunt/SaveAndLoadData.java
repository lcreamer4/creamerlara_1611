package com.laracreamer.creamerlara_scavengerhunt;


import android.content.Context;

import com.laracreamer.creamerlara_scavengerhunt.activities.MainActivity;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class SaveAndLoadData {
    private static final String FILE_NAME = "LCreamer_CE07";

    private final Context context;

    public SaveAndLoadData(Context context) {
        this.context = context;
    }

    public void saveData(ArrayList<String> saveArray) {
        try {
            //The file output steam is created with the file name the user provided, with
            //Whether they want the user to be able to locate this info from other apps.
            FileOutputStream fos = context.openFileOutput(FILE_NAME, MainActivity.MODE_PRIVATE);
            //An object output is created to pass the whole array to the file.
            ObjectOutput oos = new ObjectOutputStream(fos);
            //This object writes the data to the array in the argument.
            oos.writeObject(saveArray);
            //Close the stream.
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    public ArrayList<String> loadData() {

        ArrayList<String> loadedArray = null;

        try {
            FileInputStream fis = context.openFileInput(FILE_NAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            loadedArray = (ArrayList<String>) ois.readObject();
            ois.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return loadedArray;
    }

}
