package com.laracreamer.creamerlara_scavengerhunt.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.fragaments.GameFragment;
import java.util.ArrayList;


public class GameActivity extends AppCompatActivity {

    String EXTRA_QUESTIONS = "com.fullsail.android.EXTRA_QUESTIONS";
    String EXTRA_ANSWERS = "com.fullsail.android.EXTRA_ANSWERS";
    String EXTRA_PARK_CHOSEN = "con.fullsail.android.EXTRA_PARK_CHOSEN";
    String EXTRA_LATITUDES = "com.fullsail.android.EXTRA_LATITUDES";
    String EXTRA_LONGITUDES = "com.fullsail.android.EXTRA_LONGITUDES";
    ArrayList<String> questions;
    ArrayList<String> answers;
    ArrayList<String> latitudes;
    ArrayList<String> longitudes;
    String parkChosen;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Grab the values passed through in the intent, which will then be passed through to the game fragment.
        Intent intent = getIntent();
        if(intent.hasExtra(EXTRA_QUESTIONS) && intent.hasExtra(EXTRA_ANSWERS) && intent.hasExtra(EXTRA_LATITUDES) && intent.hasExtra(EXTRA_LONGITUDES) && intent.hasExtra(EXTRA_PARK_CHOSEN)) {
            questions = intent.getStringArrayListExtra(EXTRA_QUESTIONS);
            answers = intent.getStringArrayListExtra(EXTRA_ANSWERS);
            latitudes = intent.getStringArrayListExtra(EXTRA_LATITUDES);
            longitudes = intent.getStringArrayListExtra(EXTRA_LONGITUDES);
            parkChosen = intent.getStringExtra(EXTRA_PARK_CHOSEN);
        }

        //Have the fragment replace the main container.
        GameFragment gameFragment = GameFragment.newInstance(questions, answers, latitudes, longitudes, parkChosen);
        getFragmentManager().beginTransaction().replace(R.id.container, gameFragment, GameFragment.TAG).commit();
    }

}
