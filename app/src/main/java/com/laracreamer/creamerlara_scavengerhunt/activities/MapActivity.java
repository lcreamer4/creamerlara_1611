package com.laracreamer.creamerlara_scavengerhunt.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;

import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.fragaments.CustomMapFragment;


public class MapActivity extends AppCompatActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create the instance of the first mapFragment, and have it appear.
        CustomMapFragment customMapFragment = CustomMapFragment.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.container, customMapFragment, customMapFragment.TAG).commit();

    }

}
