package com.laracreamer.creamerlara_scavengerhunt.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.fragaments.ThirdRulesFragment;


public class ThirdRulesActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Create the instance for the third rules fragment, and have it appear.
        ThirdRulesFragment thirdRulesFragment = ThirdRulesFragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.container, thirdRulesFragment, ThirdRulesFragment.TAG).commit();

    }
}
