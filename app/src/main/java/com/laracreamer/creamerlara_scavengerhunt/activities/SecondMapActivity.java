package com.laracreamer.creamerlara_scavengerhunt.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.fragaments.SecondMapFragment;


public class SecondMapActivity extends AppCompatActivity {

    String EXTRA_CORRECT_LAT = "com.fullsail.android.EXTRA_CORRECT_LAT";
    String EXTRA_CORRECT_LONG = "com.fullsail.android.EXTRA_CORRECT_LONG";
    String latitude;
    String longitude;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Grab the values passed through in the intent, which will then be passed into the second map fragment.
        if(getIntent().hasExtra(EXTRA_CORRECT_LAT) && getIntent().hasExtra(EXTRA_CORRECT_LONG)) {
            latitude = getIntent().getStringExtra(EXTRA_CORRECT_LAT);
            longitude = getIntent().getStringExtra(EXTRA_CORRECT_LONG);

        }

        //Create the instance of the second map fragment, and have it appear.
        SecondMapFragment secondMapFragment = SecondMapFragment.newInstance(Double.parseDouble(latitude), Double.parseDouble(longitude));
        getSupportFragmentManager().beginTransaction().replace(R.id.container, secondMapFragment, secondMapFragment.TAG).commit();

    }
}
