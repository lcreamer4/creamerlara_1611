package com.laracreamer.creamerlara_scavengerhunt.activities;

import android.content.pm.ActivityInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.fragaments.HomeScreenFragment;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set the orientation to portrait
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //Create the instance of the home screen fragment and have it appear.
        HomeScreenFragment homeScreenFragment = HomeScreenFragment.newInstance();
        getFragmentManager().beginTransaction().replace(R.id.container, homeScreenFragment, HomeScreenFragment.TAG).commit();

    }
}
