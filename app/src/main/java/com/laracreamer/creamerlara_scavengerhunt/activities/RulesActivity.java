package com.laracreamer.creamerlara_scavengerhunt.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.fragaments.HomeScreenFragment;
import com.laracreamer.creamerlara_scavengerhunt.fragaments.RulesFragment;

public class RulesActivity extends AppCompatActivity {

 @Override
protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

     //Create the instance for the first rules activity, and have it appear.
    RulesFragment rulesFragment = RulesFragment.newInstance();
    getFragmentManager().beginTransaction().replace(R.id.container, rulesFragment, HomeScreenFragment.TAG).commit();

    }
}

