package com.laracreamer.creamerlara_scavengerhunt.fragaments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.activities.ThirdRulesActivity;


public class SecondRulesFragment extends Fragment {

    public static final String TAG = "SecondRulesFragment.TAG";
    RelativeLayout mainLayout;
    Button nextButton;

    //Create the new instance for the Second Rules Fragment
    public static SecondRulesFragment newInstance(){
        return new SecondRulesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return inflater.inflate(R.layout.second_rules_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getView() != null) {

            //Assign the next button and layout to the correct ids.
            nextButton = (Button)getView().findViewById(R.id.nextButton);
            mainLayout = (RelativeLayout)getView().findViewById(R.id.mainLayout);

    nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Close the activity, and open the activity for the third set of rules.
                    getActivity().finish();
                    Intent intent = new Intent(getActivity(), ThirdRulesActivity.class);
                    startActivity(intent);
                }
            });
        }
     }
 }

