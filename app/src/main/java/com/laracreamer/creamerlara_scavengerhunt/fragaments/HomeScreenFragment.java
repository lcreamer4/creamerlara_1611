package com.laracreamer.creamerlara_scavengerhunt.fragaments;

import android.Manifest;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.activities.MapActivity;
import com.laracreamer.creamerlara_scavengerhunt.activities.RulesActivity;



public class HomeScreenFragment extends Fragment {

    public static final String TAG = "HomeScreenFragment.TAG";
    Button scavengerButton;
    ImageView rulesButton;

    //Create the new instance of the home screen fragment.
    public static HomeScreenFragment newInstance(){
        return new HomeScreenFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_menu_layout, container, false);
    }




    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Request the permissions to use the map location here so that when the user clicks the start scavenging button, the permission has already been granted.
        ActivityCompat.requestPermissions(getActivity(), new String[]{
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION}, 1);


            if (getView() != null) {
                scavengerButton = (Button) getView().findViewById(R.id.startScavengerHunt);
                rulesButton = (ImageView) getView().findViewById(R.id.rulesHomeScreen);

                scavengerButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Create the intent for opening the map activity, and open the activity.
                        Intent intent = new Intent(getActivity(), MapActivity.class);
                        startActivity(intent);
                    }
                });

                rulesButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Create the intent for opening the rules activity, and open the activity.
                        Intent intent = new Intent(getActivity(), RulesActivity.class);
                        startActivity(intent);
                    }
                });
        }
    }
}
