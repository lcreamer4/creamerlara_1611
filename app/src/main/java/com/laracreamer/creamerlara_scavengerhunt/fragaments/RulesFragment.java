package com.laracreamer.creamerlara_scavengerhunt.fragaments;


import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.activities.SecondRulesActivity;

public class RulesFragment extends Fragment {

    public static final String TAG = "RulesFragment.TAG";
    Button nextButton;

    //Create the new instance for the rules fragment.
    public static RulesFragment newInstance(){
        return new RulesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.rules_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getView() != null) {
            nextButton = (Button)getView().findViewById(R.id.nextButton);

            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Close this set on rules, and open the second rules activity.
                    getActivity().finish();
                    Intent intent = new Intent(getActivity(), SecondRulesActivity.class);
                    startActivity(intent);
                }
            });
        }
    }
}
