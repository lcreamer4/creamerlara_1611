package com.laracreamer.creamerlara_scavengerhunt.fragaments;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import com.laracreamer.creamerlara_scavengerhunt.R;

public class ThirdRulesFragment extends Fragment {
    public static final String TAG = "ThirdRulesFragment.TAG";
    RelativeLayout mainLayout;
    Button nextButton;

    //Create the new instance for the third rules fragment.
    public static ThirdRulesFragment newInstance(){
        return new ThirdRulesFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.third_rules_layout, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(getView() != null) {

            //Assign the next button and the main layout to their correct ids.
            nextButton = (Button)getView().findViewById(R.id.nextButton);
            nextButton.setText("Done");
            mainLayout = (RelativeLayout)getView().findViewById(R.id.mainLayout);

            nextButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Close this activity, and the app will return to either the main screen
                    // or the game screen, depending on which one the user originally came from
                    getActivity().finish();
                }
            });
        }
    }
}
