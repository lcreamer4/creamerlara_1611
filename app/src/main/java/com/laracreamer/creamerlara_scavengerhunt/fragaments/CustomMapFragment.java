package com.laracreamer.creamerlara_scavengerhunt.fragaments;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.GameEntity;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.activities.GameActivity;

import java.util.ArrayList;


public class CustomMapFragment extends SupportMapFragment  {

    public static GoogleMap mMap;
    public final String TAG = "CustomMapFragment.TAG";
    String currentLat = "Default";
    String currentLong = "Default";
    LatLng myLatLong;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    String currentNumber = "0";
    String park = "";
    ArrayList<String> parkQuestions = new ArrayList<>();
    ArrayList<String> parkAnswers = new ArrayList<>();
    ArrayList<String> parkLatitudes = new ArrayList<>();
    ArrayList<String> parkLongitudes = new ArrayList<>();
    String EXTRA_QUESTIONS = "com.fullsail.android.EXTRA_QUESTIONS";
    String EXTRA_ANSWERS = "com.fullsail.android.EXTRA_ANSWERS";
    String EXTRA_LATITUDES = "com.fullsail.android.EXTRA_LATITUDES";
    String EXTRA_LONGITUDES = "com.fullsail.android.EXTRA_LONGITUDES";
    String EXTRA_PARK_CHOSEN = "con.fullsail.android.EXTRA_PARK_CHOSEN";
    String parkChosen = "";

    //Create the new instance for the custom map fragment.
    public static CustomMapFragment newInstance() {
        return new CustomMapFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onStart() {
        super.onStart();

        //Set the park and the park chosen to empty strings.
        park = "";
        parkChosen = "";
    }


    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        //Get the map Async, so that the map appears.
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                //Assign the global variable to the google map.
                mMap = googleMap;

                //Check to see that the user allowed for the map to get the current position.
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    //If the user allowed permissions, set the current location and have the map zoom in on the users current location.
                    mMap.setMyLocationEnabled(true);
                    LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    Criteria criteria = new Criteria();
                    String provider = locationManager.getBestProvider(criteria, true);
                    Location myLocation = locationManager.getLastKnownLocation(provider);
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                    //Check to make sure that the location is not null before continuing on.
                    if (myLocation != null) {

                        // Create a LatLng object for the current location
                        double latitude = myLocation.getLatitude();
                        double longitude = myLocation.getLongitude();

                        myLatLong = new LatLng(latitude, longitude);
                        currentLat = String.valueOf(latitude);
                        currentLong = String.valueOf(longitude);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLatLong));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                    }

                    //Set the latitude and longitude for the four parks.
                    final Double mkLatitude = 28.415870;
                    final Double mkLongitude = -81.581193;

                    final Double epLatitude = 28.376330;
                    final Double epLongitude = -81.549417;

                    final Double hsLatitude = 28.358411;
                    final Double hsLongitude = -81.558678;

                    final Double akLatitude = 28.355378;
                    final Double akLongitude = -81.590120;

                    final Double fsLatitude = 28.590945;
                    final Double fsLongitude = -81.304598;

                    //Create the circles on the map, which will cover each of the four theme parks, as well as full sail.
                    Circle mkCircle = mMap.addCircle(new CircleOptions().center(new LatLng(mkLatitude, mkLongitude)).strokeColor(R.color.colorGeofence).fillColor(R.color.colorGeofence).radius(100));
                    Circle epCircle = mMap.addCircle(new CircleOptions().center(new LatLng(epLatitude, epLongitude)).strokeColor(R.color.colorGeofence).fillColor(R.color.colorGeofence).radius(100));
                    Circle hsCircle = mMap.addCircle(new CircleOptions().center(new LatLng(hsLatitude, hsLongitude)).strokeColor(R.color.colorGeofence).fillColor(R.color.colorGeofence).radius(100));
                    Circle akCircle = mMap.addCircle(new CircleOptions().center(new LatLng(akLatitude, akLongitude)).strokeColor(R.color.colorGeofence).fillColor(R.color.colorGeofence).radius(100));
                    Circle fsCircle = mMap.addCircle(new CircleOptions().center(new LatLng(fsLatitude, fsLongitude)).strokeColor(R.color.colorGeofence).fillColor(R.color.colorGeofence).radius(2000));

                    if(myLocation != null) {

                        //Create a float which will hold the users latitude and longitude.
                        float[] distance = new float[2];

                        //Use the built in Location.distanceBetween built into Google Maps API to see how far away the user is from the full sail circle.
                        Location.distanceBetween(myLatLong.latitude, myLatLong.longitude,
                                fsCircle.getCenter().latitude, fsCircle.getCenter().longitude, distance);

                        //If the user is outside of the  full sail circle, send them a toast notifying them of that.
                        if (distance[0] > fsCircle.getRadius()) {
                            Toast.makeText(getActivity(), "You are too far away", Toast.LENGTH_SHORT).show();

                            //Otherwise, let them know that they are in the right spot, and assign the correct park.
                        } else {
                            Toast.makeText(getActivity(), "You are in the right spot. Starting the game.", Toast.LENGTH_SHORT).show();
                            park = "Epcot";
                            parkChosen = "Epcot";
                        }

                        //Set up the check for the magic kingdom circle.
                        Location.distanceBetween(myLatLong.latitude, myLatLong.longitude,
                                mkCircle.getCenter().latitude, mkCircle.getCenter().longitude, distance);

                        //Repeat the same check for the magic kingdom circle.
                        if (distance[0] > mkCircle.getRadius()) {
                            Log.i(TAG, "You are too far away from Magic Kingdom");
                        } else {
                            park = "Magic Kingdom";
                            parkChosen = "Magic Kingdom";
                        }


                        //Set up the check for the epcot circle.
                        Location.distanceBetween(myLatLong.latitude, myLatLong.longitude,
                                epCircle.getCenter().latitude, epCircle.getCenter().longitude, distance);

                        //Repeat the same check for the epcot circle.
                        if (distance[0] > epCircle.getRadius()) {
                            Log.i(TAG, "You are too far away from Epcot");
                        } else {
                            park = "Epcot";
                            parkChosen = "Epcot";
                        }

                        //Set up the check for the hollywood studios circle.
                        Location.distanceBetween(myLatLong.latitude, myLatLong.longitude,
                                hsCircle.getCenter().latitude, hsCircle.getCenter().longitude, distance);

                        //Repeat the same check for the hollywood studios circle.
                        if (distance[0] > hsCircle.getRadius()) {
                            Log.i(TAG, "You are too far away from Hollywood Studios");
                        } else {
                            park = "Hollywood Studios";
                            parkChosen = "Hollywood Studios";
                        }

                        //Set up the check for the animal kingdom circle.
                        Location.distanceBetween(myLatLong.latitude, myLatLong.longitude,
                                akCircle.getCenter().latitude, akCircle.getCenter().longitude, distance);

                        //Repeat the same check for the animal kingdom circle.
                        if (distance[0] > akCircle.getRadius()) {
                            Log.i(TAG, "You are too far away from Animal Kingdom");
                        } else {
                            park = "Animal Kingdom";
                            parkChosen = "Animal Kingdom";
                        }


                        //Only create one instance of the database in order to update all the information.
                        DatabaseReference database = FirebaseDatabase.getInstance().getReferenceFromUrl("https://scavenger-hunt-app-4f9c2.firebaseio.com");

                        database.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                //Set up the current number so that it matches the way the numbers are laid out in the database,
                                //Basically any number under 10 has to have a 0 in front of it.
                                for (int i = 1; i < 11; i++) {
                                    if (i < 10) {
                                        currentNumber = "0" + String.valueOf(i);
                                    } else if (i >= 10) {
                                        currentNumber = String.valueOf(i);
                                    }


                                    //Check to see if the park equals Magic Kingdom, if it does, pull the correct information, and assign it to arrays.
                                    if (park.equals("Magic Kingdom")) {
                                        parkQuestions.add(String.valueOf(dataSnapshot.child("Magic Kingdom").child("01: Questions").child("Question " + currentNumber).getValue().toString()));
                                        parkAnswers.add(String.valueOf(dataSnapshot.child("Magic Kingdom").child("02: Answers").child("Answer " + currentNumber).getValue().toString()));
                                        parkLatitudes.add(String.valueOf(dataSnapshot.child("Magic Kingdom").child("03: GPS Location").child("Question " + currentNumber + " Position").child("Latitude").getValue().toString()));
                                        parkLongitudes.add(String.valueOf(dataSnapshot.child("Magic Kingdom").child("03: GPS Location").child("Question " + currentNumber + " Position").child("Longitude").getValue().toString()));
                                        getActivity().finish();

                                        //Check to see if the park equals Epcot, if it does, pull the correct information, and assign it to arrays.
                                    } else if (park.equals("Epcot")) {
                                        parkQuestions.add(String.valueOf(dataSnapshot.child("Epcot").child("01: Questions").child("Question " + currentNumber).getValue().toString()));
                                        parkAnswers.add(String.valueOf(dataSnapshot.child("Epcot").child("02: Answers").child("Answer " + currentNumber).getValue().toString()));
                                        parkLatitudes.add(String.valueOf(dataSnapshot.child("Epcot").child("03: GPS Location").child("Question " + currentNumber + " Position").child("Latitude").getValue().toString()));
                                        parkLongitudes.add(String.valueOf(dataSnapshot.child("Epcot").child("03: GPS Location").child("Question " + currentNumber + " Position").child("Longitude").getValue().toString()));
                                        getActivity().finish();

                                        //Check to see if the park equals Hollywood Studios, if it does, pull the correct information, and assign it to arrays.
                                    } else if (park.equals("Hollywood Studios")) {
                                        parkQuestions.add(String.valueOf(dataSnapshot.child("Hollywood Studios").child("01: Questions").child("Question " + currentNumber).getValue().toString()));
                                        parkAnswers.add(String.valueOf(dataSnapshot.child("Hollywood Studios").child("02: Answers").child("Answer " + currentNumber).getValue().toString()));
                                        parkLatitudes.add(String.valueOf(dataSnapshot.child("Hollywood Studios").child("03: GPS Location").child("Question " + currentNumber + " Position").child("Latitude").getValue().toString()));
                                        parkLongitudes.add(String.valueOf(dataSnapshot.child("Hollywood Studios").child("03: GPS Location").child("Question " + currentNumber + " Position").child("Longitude").getValue().toString()));
                                        getActivity().finish();

                                        //Check to see if the park equals Animal Kingdom, if it does, pull the correct information, and assign it to arrays.
                                    } else if (park.equals("Animal Kingdom")) {
                                        parkQuestions.add(String.valueOf(dataSnapshot.child("Animal Kingdom").child("01: Questions").child("Question " + currentNumber).getValue().toString()));
                                        parkAnswers.add(String.valueOf(dataSnapshot.child("Animal Kingdom").child("02: Answers").child("Answer " + currentNumber).getValue().toString()));
                                        parkLatitudes.add(String.valueOf(dataSnapshot.child("Animal Kingdom").child("03: GPS Location").child("Question " + currentNumber + " Position").child("Latitude").getValue().toString()));
                                        parkLongitudes.add(String.valueOf(dataSnapshot.child("Animal Kingdom").child("03: GPS Location").child("Question " + currentNumber + " Position").child("Longitude").getValue().toString()));
                                        getActivity().finish();

                                    }
                                }

                                //Create the intent which will open the game activity and start the scavenger hunt.
                                //Put in all of the arrays with the data as extras, which will be captured in the game activity and passed to the game fragment.
                                Intent intent = new Intent(getActivity(), GameActivity.class);
                                intent.putExtra(EXTRA_QUESTIONS, parkQuestions);
                                intent.putExtra(EXTRA_ANSWERS, parkAnswers);
                                intent.putExtra(EXTRA_LATITUDES, parkLatitudes);
                                intent.putExtra(EXTRA_LONGITUDES, parkLongitudes);
                                intent.putExtra(EXTRA_PARK_CHOSEN, parkChosen);
                                startActivity(intent);
                            }


                            @Override
                            public void onCancelled(DatabaseError error) {
                                // Failed to read value
                                Log.w("Failed to read value.", error.toException());
                            }
                        });

                        //This else statement is used if the users current location is null.
                    } else {
                        Toast.makeText(getActivity(), "Cannot find your current location, hit the target in the right hand corner to pin point your location, and then walk towards one of the geofences.", Toast.LENGTH_LONG).show();
                    }

                    //This else statement is used if the user did not allow permissions for accessing their current location.
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                }
            }
        });
    }
}
