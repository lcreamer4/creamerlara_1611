package com.laracreamer.creamerlara_scavengerhunt.fragaments;


import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.laracreamer.creamerlara_scavengerhunt.R;
import com.laracreamer.creamerlara_scavengerhunt.SaveAndLoadData;
import com.laracreamer.creamerlara_scavengerhunt.activities.MainActivity;
import com.laracreamer.creamerlara_scavengerhunt.activities.RulesActivity;
import com.laracreamer.creamerlara_scavengerhunt.activities.SecondMapActivity;
import java.util.ArrayList;
import java.util.Random;

import static android.app.Activity.RESULT_OK;

public class GameFragment extends Fragment implements View.OnClickListener {

public static final String TAG = "GameFragment.TAG";
private static final String ARG_QUESTIONS = "GameFragment.QUESTIONS";
private static final String ARG_ANSWERS = "GameFragment.ANSWERS";
private static final String ARG_LATITUDES = "GameFragment.LATITUDES";
private static final String ARG_LONGITUDES = "GameFragment.LONGITUDES";
private static final String ARG_PARK_CHOSEN = "GameFragment.PARK_CHOSEN";
private  static final String EXTRA_CORRECT_LAT = "com.fullsail.android.EXTRA_CORRECT_LAT";
private static final String EXTRA_CORRECT_LONG = "com.fullsail.android.EXTRA_CORRECT_LONG";
int currentQuestion = 0;
    TextView questionTextView;
    ArrayList<String> questions = new ArrayList<>();
    ArrayList<String> answers = new ArrayList<>();
    ArrayList<String> latitudes = new ArrayList<>();
    ArrayList<String> longitudes = new ArrayList<>();
    ArrayList<String> randomAnswers = new ArrayList<>();
    ArrayList<String> shuffledAnswers = new ArrayList<>();
    SaveAndLoadData save;
    ImageView home;
    ImageView rules;
    String buttonText;
    String park;
    Context context;
    Button answer1;
    Button answer2;
    Button answer3;
    Button checkLocation;
    String correctLat;
    String correctLong;
    String hardcodedLat = "28.590945";
    String hardcodedLong = "-81.304598";

    //For the new instance of the game fragment, grab all of the values passed in and assign them to arguments.
public static GameFragment newInstance(ArrayList<String> questions, ArrayList<String> answers, ArrayList<String> latitudes, ArrayList<String> longitudes, String parkChosen) {
    GameFragment gameFragment = new GameFragment();
    Bundle args = new Bundle();
    args.putSerializable(ARG_QUESTIONS, questions);
    args.putSerializable(ARG_ANSWERS, answers);
    args.putSerializable(ARG_LATITUDES, latitudes);
    args.putSerializable(ARG_LONGITUDES, longitudes);
    args.putSerializable(ARG_PARK_CHOSEN, parkChosen);
    gameFragment.setArguments(args);
    return gameFragment;
}

@Override
public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.game_layout, container, false);
    save = new SaveAndLoadData(getActivity());
    setHasOptionsMenu(true);
    context = getActivity();


    //If the view is not null, assign all of the text views, image views to the correct id, and assign the array lists to the arguments.
    if(view != null) {
        questionTextView = (TextView) view.findViewById(R.id.questionTextView);
        home = (ImageView) view.findViewById(R.id.homescreenIcon);
        rules = (ImageView)view.findViewById(R.id.rules);
        questions = getArguments().getStringArrayList(ARG_QUESTIONS);
        answers = getArguments().getStringArrayList(ARG_ANSWERS);
        latitudes = getArguments().getStringArrayList(ARG_LATITUDES);
        longitudes = getArguments().getStringArrayList(ARG_LONGITUDES);
        park = getArguments().getString(ARG_PARK_CHOSEN);

        //Set the text of the text view to the questions array,  indexed by the current question.
        questionTextView.setText(questions.get(currentQuestion));
        //Assign the correct latitude for the answer to the latitude array, indexed by the current question.
        correctLat = latitudes.get(currentQuestion);
        //Assign the correct longitude for the answer to the longitude array, indexed by the current question.
        correctLong = longitudes.get(currentQuestion);

        //Assign the buttons to their correct ids.
        answer1 = (Button) view.findViewById(R.id.answer1);
        answer2 = (Button) view.findViewById(R.id.answer2);
        answer3 = (Button) view.findViewById(R.id.answer3);
        checkLocation = (Button)view.findViewById(R.id.checkLocation);

        answer1.setOnClickListener(this);
        answer2.setOnClickListener(this);
        answer3.setOnClickListener(this);
        checkLocation.setOnClickListener(this);
        home.setClickable(true);
        home.setOnClickListener(this);
        rules.setClickable(true);
        rules.setOnClickListener(this);

        //Save the answers array, since it will be shuffled and changed in the future, then shuffle nad reshuffle the array.
        save.saveData(answers);
        shuffleArray();
        reShuffleArray();


        //Once the answers are shuffled, assign them to the next of the three buttons.
        answer1.setText(shuffledAnswers.get(0));
        answer2.setText(shuffledAnswers.get(1));
        answer3.setText(shuffledAnswers.get(2));

        //When the game screen opens, the three options should not be visible until the user checks their location.
        answer1.setVisibility(View.GONE);
        answer2.setVisibility(View.GONE);
        answer3.setVisibility(View.GONE);
    }

    return view;
}

@Override
public void onClick(View v) {
    if(v.getId() == R.id.answer1) {
        //Assign the button text to what the text of the button is.
        buttonText = answer1.getText().toString();
        //Set clickable or enabled to buttons to false to prevent the user from clicking on all buttons again.
        answer1.setClickable(false);
        answer2.setClickable(false);
        answer3.setClickable(false);
        //Check the answer to see if it is correct.
        checkAnswer(buttonText, currentQuestion, park);

    } else if (v.getId() == R.id.answer2) {
        //Assign the button text to what the text of the button is.
        buttonText = answer2.getText().toString();
        //Set clickable or enabled to buttons to false to prevent the user from clicking on all buttons again.
        answer1.setClickable(false);
        answer2.setClickable(false);
        answer3.setClickable(false);
        //Check the answer to see if it is correct.
        checkAnswer(buttonText, currentQuestion, park);

    } else if (v.getId() == R.id.answer3) {
        //Assign the button text to what the text of the button is.
        buttonText = answer3.getText().toString();
        //Set clickable or enabled to buttons to false to prevent the user from clicking on all buttons again.
        answer1.setClickable(false);
        answer2.setClickable(false);
        answer3.setClickable(false);
        //Check the answer to see if it is correct.
        checkAnswer(buttonText, currentQuestion, park);

    } else if (v.getId() == R.id.homescreenIcon) {
        //Create an alert dialogue which will check whether the user wants to leave the game or not.
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Leave Game");
        alert.setMessage("Are you sure you want to leave the scavenger hunt? All of your current progress will be lost.");

        //If the user clicks ok, then close the activity and open the activity for the main menu screen.
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                getActivity().finish();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });

        //If the user clicks the cancel button, nothing happens, and the alert dialogue closes.
        alert.setNegativeButton("Cancel", null);

        //Show the alert.
        alert.show();
    } else if (v.getId() == R.id.rules) {

        //Open the first rules activity, and do not close out of the game.
        Intent intent = new Intent(getActivity(), RulesActivity.class);
        startActivity(intent);

    } else if (v.getId() == R.id.checkLocation) {
        //Create an intent which will open the activity for the second map activity.
        //Put the latitude and longitude for the correct answers in as extras.
        //This will be changed when released so that the correct lat and long are the answers to the questons being asked.
        //The variables are there, but are not used for now, because otherwise you would not be able to check the location and get a correct location response.
        Intent intent = new Intent(getActivity(), SecondMapActivity.class);
        intent.putExtra(EXTRA_CORRECT_LAT, hardcodedLat);
        intent.putExtra(EXTRA_CORRECT_LONG, hardcodedLong);
        startActivityForResult(intent, 0);
    }
}

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //After the location is checked, if the result code is ok, then the user is in the right spot.
        if(resultCode == RESULT_OK) {
            //Change the buttons that are visible to the user so that they can choose from the multiple choice answers.
           checkLocation.setVisibility(View.GONE);
            answer1.setVisibility(View.VISIBLE);
            answer2.setVisibility(View.VISIBLE);
            answer3.setVisibility(View.VISIBLE);
        } else {
            //Otherwise, the user is not in the right spot, so there is a toast to tell them that they are not in the right location.
            Toast.makeText(getActivity(), "You are in the wrong spot, try again", Toast.LENGTH_SHORT).show();
        }
    }

    //This is the function which shuffles the answers for the three buttons.
    public ArrayList<String> shuffleArray() {
    randomAnswers = new ArrayList<>();
    //Add the correct answer to the array.
    randomAnswers.add(answers.get(currentQuestion));
    //Remove that answer form the answers array.
    answers.remove(currentQuestion);
    //Shuffle the array
    for (int i = 0; i < 2; i++) {
        Random rand = new Random();
        int n = rand.nextInt(answers.size() - 1);
        randomAnswers.add(answers.get(n));
        answers.remove(n);
    }
    return randomAnswers;
}

    //This is the function that takes the three answers from the shuffle array function, and shuffles them again.
    //This ensures that the correct answer is in a different spot most of the time.
public ArrayList<String> reShuffleArray() {
    shuffledAnswers = new ArrayList<>();
    //Re-shuffle the array so that the answers are not in the same order.
    for (int i = 0; i < 3; i++) {
        Random rand = new Random();
        int n = rand.nextInt(randomAnswers.size());
        shuffledAnswers.add(randomAnswers.get(n));
        randomAnswers.remove(n);
    }
    return shuffledAnswers;
}

    //Reset the origional array of answers to the array that was saved earlier.
public ArrayList<String> resetArray() {
    if(save.loadData() != null) {
        answers = save.loadData();
    }
    return answers;
}

    //This is the check answer function, which checks each question to see if the button that the user clicked contains the correct answer.
public void checkAnswer(String buttonText, int currentQuestion, String currentPark) {
    if (currentPark.equals("Magic Kingdom")) {
        if (currentQuestion == 0) {
            if (buttonText.contains("Partners")
                    || buttonText.contains("Walt")
                    && buttonText.contains("Mickey")) {
                //Place the correct alert dialogue here.
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 1) {
            if (buttonText.contains("Cinderella")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 2) {
            if (buttonText.contains("Carousel") && buttonText.contains("Progress")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 3) {
            if (buttonText.contains("Mad") && buttonText.contains("Teacups") || buttonText.contains("Teaparty")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 4) {
            if (buttonText.contains("Seven") || buttonText.contains("Dwarfs") || buttonText.contains("Mine") && buttonText.contains("Train")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 5) {
            if (buttonText.contains("Haunted") && buttonText.contains("Mansion")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 6) {
            if (buttonText.contains("Muppets") || buttonText.contains("Great") || buttonText.contains("Moments") || buttonText.contains("American") || buttonText.contains("History")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 7) {
            if (buttonText.contains("Splash") && buttonText.contains("Mountain")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 8) {
            if (buttonText.contains("Jungle") && buttonText.contains("Cruise")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 9) {
            if (buttonText.contains("Train")) {
                finalCorrectAlert();
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        }
    } else if (currentPark.equals("Hollywood Studios")) {
        if (currentQuestion == 0) {
            if (buttonText.contains("Great")
                    || buttonText.contains("Movie")) {
                //Place the correct alert dialogue here.
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 1) {
            if (buttonText.contains("Mermaid")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 2) {
            if (buttonText.contains("Tower") || buttonText.contains("Terror")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 3) {
            if (buttonText.contains("Rockin")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 4) {
            if (buttonText.contains("Beauty")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 5) {
            if (buttonText.contains("Writer")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 6) {
            if (buttonText.contains("Star")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 7) {
            if (buttonText.contains("Indiana")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 8) {
            if (buttonText.contains("Wonderful")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 9) {
            if (buttonText.contains("Fantasmic")) {
                finalCorrectAlert();
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        }
    } else if (currentPark.equals("Epcot")) {
        if (currentQuestion == 0) {
            if (buttonText.contains("Spaceship")
                    || buttonText.contains("Earth")) {
                //Place the correct alert dialogue here.
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 1) {
            if (buttonText.contains("Soarin")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 2) {
            if (buttonText.contains("Seas") || buttonText.contains("Nemo")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 3) {
            if (buttonText.contains("Norway")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 4) {
            if (buttonText.contains("Italy")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 5) {
            if (buttonText.contains("Morocco")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 6) {
            if (buttonText.contains("France")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 7) {
            if (buttonText.contains("Canada")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 8) {
            if (buttonText.contains("Test")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 9) {
            if (buttonText.contains("Ellen")) {
                finalCorrectAlert();
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        }
    } else if (currentPark.equals("Animal Kingdom")) {
        if (currentQuestion == 0) {
            if (buttonText.contains("Tree")
                    || buttonText.contains("Life")) {
                //Place the correct alert dialogue here.
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 1) {
            if (buttonText.contains("Safari")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 2) {
            if (buttonText.contains("Planet") || buttonText.contains("Watch")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 3) {
            if (buttonText.contains("Lion") || buttonText.contains("King")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 4) {
            if (buttonText.contains("Flights")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 5) {
            if (buttonText.contains("Kali")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 6) {
            if (buttonText.contains("Everest")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 7) {
            if (buttonText.contains("Dinosaur")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 8) {
            if (buttonText.contains("Flame")) {
                correctAlert();
                answer1.setEnabled(true);
                answer2.setEnabled(true);
                answer3.setEnabled(true);
                answer1.setClickable(true);
                answer2.setClickable(true);
                answer3.setClickable(true);
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        } else if (currentQuestion == 9) {
            if (buttonText.contains("Bugs")) {
                finalCorrectAlert();
            } else {
                incorrectAlert();
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setClickable(false);
                    answer2.setClickable(true);
                    answer3.setClickable(true);
                } else if (buttonText.equals(answer2.getText().toString())) {
                    answer2.setClickable(false);
                    answer3.setClickable(true);
                    answer1.setClickable(true);
                } else if (buttonText.equals(answer3.getText().toString())) {
                    answer3.setClickable(false);
                    answer1.setClickable(true);
                    answer2.setClickable(true);
                }
            }
        }
    }
}

    //This is the functionality for if the user got the answer correct.
public void correctAlert() {
    currentQuestion++;
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setTitle("That's Right!");
    alert.setMessage("You got the answer correct");

    alert.setPositiveButton("Next Question", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {

            //Re-shuffle the arrays so that there are new answers.
            resetArray();
            shuffleArray();
            reShuffleArray();

            //Once the answers are shuffled, assign them to the next of the three buttons.
            answer1.setText(shuffledAnswers.get(0));
            answer2.setText(shuffledAnswers.get(1));
            answer3.setText(shuffledAnswers.get(2));

            answer1.setVisibility(View.GONE);
            answer2.setVisibility(View.GONE);
            answer3.setVisibility(View.GONE);

            checkLocation.setVisibility(View.VISIBLE);

            //Assign the question text view to the current question.
            questionTextView.setText(questions.get(currentQuestion));
        }
    });
    alert.show();
}

    //This is the functionality for if the user gor the answer correct for the final question.
public void finalCorrectAlert() {
    currentQuestion++;
    AlertDialog.Builder alert = new AlertDialog.Builder(context);
    alert.setTitle("You Did It!");
    alert.setMessage("You've completed the scavenger hunt!");

    alert.setPositiveButton("Back to Main Menu", new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
           getActivity().finish();

        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);

        }
    });
    alert.show();
}

    //This is the functionality for if the user got the question incorrect
public void incorrectAlert() {
    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Not Quite!");
        alert.setMessage("You're Answer is incorrect, try again!");
        alert.setPositiveButton("Try Again", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(buttonText.equals(answer1.getText().toString())) {
                    answer1.setEnabled(false);
                    answer1.setClickable(false);
                } else if(buttonText.equals(answer2.getText().toString())) {
                    answer2.setEnabled(false);
                    answer2.setClickable(false);
                } else if(buttonText.equals(answer3.getText().toString())) {
                    answer3.setEnabled(false);
                    answer3.setClickable(false);
                }
            }
        });
        alert.show();
    }
}



