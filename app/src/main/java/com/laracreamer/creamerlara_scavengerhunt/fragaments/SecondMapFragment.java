package com.laracreamer.creamerlara_scavengerhunt.fragaments;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.laracreamer.creamerlara_scavengerhunt.R;


import static android.app.Activity.RESULT_OK;

public class SecondMapFragment extends SupportMapFragment {

    static String ARG_ANSWER_LAT = "com.fullsail.android.ARG_ANSWER_LAT";
    static String ARG_ANSWER_LNG = "com.fullsail.android.ARG_ANSWER_LNG";
    public static GoogleMap mMap;
    public final String TAG = "CustomMapFragment.TAG";
    String currentLat = "Default";
    String currentLong = "Default";
    LatLng myLatLong;
    LatLng correctAnswer;

    //Create the new instance of the second map fragment, and take in the correct latitude and longitude of the answer as arguments.
    public static SecondMapFragment newInstance(Double answerLat, Double answerLng) {
        SecondMapFragment secondMapFragment = new SecondMapFragment();
        Bundle args = new Bundle();
        args.putDouble(ARG_ANSWER_LAT, answerLat);
        args.putDouble(ARG_ANSWER_LNG, answerLng);
        secondMapFragment.setArguments(args);
        return secondMapFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //Assign the arguments for the latitude and longitude to a new LatLng variable.
        correctAnswer = new LatLng(getArguments().getDouble(ARG_ANSWER_LAT), getArguments().getDouble(ARG_ANSWER_LNG));
    }


    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);

        //Load the map.
        getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                //Check to see that the user allowed for the map to get the current position.
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                    //If the user allowed permissions, set the current location and have the map zoom in on the users current location.
                    mMap.setMyLocationEnabled(true);
                    LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
                    Criteria criteria = new Criteria();
                    String provider = locationManager.getBestProvider(criteria, true);
                    Location myLocation = locationManager.getLastKnownLocation(provider);
                    mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                    //If the current location is not null
                    if (myLocation != null) {

                        // Create a LatLng object for the current location
                        double latitude = myLocation.getLatitude();
                        double longitude = myLocation.getLongitude();

                        //Assign the ariable myLatLng to the latitude nd longitude
                        myLatLong = new LatLng(latitude, longitude);

                        currentLat = String.valueOf(latitude);
                        currentLong = String.valueOf(longitude);
                        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLatLong));
                        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                    }

                    //Create the circle for where the correct answer is.
                    Circle answerCircle = mMap.addCircle(new CircleOptions().center(new LatLng(correctAnswer.latitude, correctAnswer.longitude)).strokeColor(R.color.colorGeofence).fillColor(R.color.colorGeofence).radius(2000));

                    //Create a float which will hold the users latitude and longitude.
                    float[] distance = new float[2];

                    //Use the built in Location.distanceBetween built into Google Maps API to see how far away the user is from the full sail circle.
                    Location.distanceBetween(myLatLong.latitude, myLatLong.longitude,
                            answerCircle.getCenter().latitude, answerCircle.getCenter().longitude, distance);

                    //If the user is outside of the  full sail circle, send them a toast notifying them of that.
                    if(distance[0] > answerCircle.getRadius()) {
                        Toast.makeText(getActivity(), "You are not in the right area.", Toast.LENGTH_SHORT).show();
                        getActivity().finish();

                        //Otherwise, set the result ok for the activity, and close the activity, bringing the user back to the game screen .
                    } else {
                        getActivity().setResult(RESULT_OK);
                        getActivity().finish();
                    }

                    //If the user has not approved the permissions, then request them again.
                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                }
            }
        });
    }
}

